# 导入工具包
from imutils import contours
import numpy as np
import argparse
import cv2 as cv
import myutils

# 设置参数
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True,
                help="path to input image")
ap.add_argument("-t", "--template", required=True,
                help="path to template OCR-A image")
args = vars(ap.parse_args())

# 指定信用卡类型
FIRST_NUMBER = {
    "3": "American Express",
    "4": "Visa",
    "5": "MasterCard",
    "6": "Discover Card"
}


# 绘图展示
def cv_show(name, image):
    cv.imshow(name, image)
    cv.waitKey(0)
    cv.destroyAllWindows()


# 读取模板图像
img = cv.imread(args["template"])
cv_show('template', img)
# 灰度图
ref = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
cv_show('gray', ref)
# 二值图像
# 为什么[1]?
ref = cv.threshold(ref, 10, 255, cv.THRESH_BINARY_INV)[1]
cv_show('ref', ref)

# 计算轮廓
# cv2.findContours()函数接受的参数为二值图，即黑白的（不是灰度图）,cv2.RETR_EXTERNAL只检测外轮廓，cv2.CHAIN_APPROX_SIMPLE只保留终点坐标
# 返回的list中每个元素都是图像中的一个轮廓

refCnts, hierarchy = cv.findContours(ref.copy(), cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
cv.drawContours(img, refCnts, -1, (0, 0, 255), 3)
# 显示轮廓
cv_show('contours', img)
print(np.array(refCnts).shape)
# 为什么要加[0]
refCnts = myutils.sort_contours(refCnts, method='left-to-right')[0]
digits = {}

# 遍历每一个轮廓
for (i, c) in enumerate(refCnts):
    # 计算外接矩形并且resize成合适大小
    (x, y, w, h) = cv.boundingRect(c)
    roi = ref[y:y + h, x:x + w]
    roi = cv.resize(roi, (57, 88))

    # 每个数字对应一个模板
    digits[i] = roi

# 初始化卷积核
rectKernel = cv.getStructuringElement(cv.MORPH_RECT, (9, 3))
sqKernel = cv.getStructuringElement(cv.MORPH_RECT, (5, 5))

# 读取输入图像，进行预处理
image = cv.imread(args["image"])
cv_show('image', image)
image = myutils.resize(image, width=300)
gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
cv_show('gray', gray)

# 礼帽操作，突出更明亮区域
tophat = cv.morphologyEx(gray, cv.MORPH_TOPHAT, rectKernel)
cv_show('tophat', tophat)

# 梯度
gradX = cv.Sobel(tophat, ddepth=cv.CV_32F, dx=1, dy=0,  # ksize=-1相当于用3*3的
                 ksize=-1)
gradX = np.absolute(gradX)
(minVal, maxVal) = (np.min(gradX), np.max(gradX))
gradX = (255 * ((gradX - minVal) / (maxVal - minVal)))
gradX = gradX.astype("uint8")

print(np.array(gradX).shape)
cv_show('gradX', gradX)

# 通过闭操作，将数字连接起来
gradX = cv.morphologyEx(gradX, cv.MORPH_CLOSE, rectKernel)
cv_show('gradX', gradX)
# THRESH_OTSU会自动寻找合适的阈值，适合双峰，需把阈值参数设置为0
# ????
thresh = cv.threshold(gradX, 0, 255,
                      cv.THRESH_BINARY | cv.THRESH_OTSU)[1]
cv_show('thresh', thresh)

# 再来一个闭操作
thresh = cv.morphologyEx(thresh, cv.MORPH_CLOSE, sqKernel)  # 再来一个闭操作
cv_show('thresh', thresh)

# 计算信用卡图片的数字区域轮廓
threshCnts, hierarchy = cv.findContours(thresh.copy(), cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
cnts = threshCnts
cur_img = image.copy()
cv.drawContours(cur_img, cnts, -1, (0, 0, 255), 3)
cv_show('img', cur_img)
locs = []

# 遍历轮廓
for (i, c) in enumerate(cnts):
    (x, y, w, h) = cv.boundingRect(c)
    ar = w / float(h)

    # 通过筛选，选择数字区域
    if ar > 2.5 and ar < 4.0:
        if (w > 40 and w < 55) and (h > 10 and h < 20):
            # 符合的留下来
            locs.append((x, y, w, h))

# 将符合的轮廓从左到右排序
locs = sorted(locs, key=lambda x: x[0])
output = []

# 遍历轮廓中的没一个数字
for (i, (gX, gY, gW, gH)) in enumerate(locs):
    # initialize the list of group digits
    groupOutput = []
    group = gray[gY - 5:gY + 5 + gH, gX - 5:gX + 5 + gW]
    cv_show('group1', group)
    # 预处理
    group = cv.threshold(group, 0, 255,
                         cv.THRESH_BINARY | cv.THRESH_OTSU)[1]
    cv_show('group2', group)
    # 计算每一组的轮廓
    digitCnts, hierarchy = cv.findContours(group.copy(), cv.RETR_EXTERNAL,
                                           cv.CHAIN_APPROX_SIMPLE)
    digitCnts = contours.sort_contours(digitCnts,
                                       method="left-to-right")[0]
    # 计算每一组中的一个数字
    for c in digitCnts:
        # 找到当前数值的轮廓，resize成合适的的大小
        (x, y, w, h) = cv.boundingRect(c)
        roi = group[y:y + h, x:x + w]
        roi = cv.resize(roi, (57, 88))
        cv_show('roi', roi)
        # 计算匹配得分
        scores = []

        # 在模板中计算每一个得分
        for (digit, digitRoi) in digits.items():
            # 模板匹配
            result = cv.matchTemplate(roi, digitRoi, cv.TM_CCOEFF)
            (_, score, _, _) = cv.minMaxLoc(result)
            scores.append(score)

        # 得到最合适的数字
        groupOutput.append(str(np.argmax(scores)))
    # 画出来
    cv.rectangle(image, (gX - 5, gY - 5),
                 (gX + gW + 5, gY + gH + 5), (0, 0, 255), 1)
    cv.putText(image, "".join(groupOutput), (gX, gY - 15),
               cv.FONT_HERSHEY_SIMPLEX, 0.65, (0, 0, 255), 2)

    # 得到结果
    output.extend(groupOutput)

# 打印结果
print("Credit Card Type: {}".format(FIRST_NUMBER[output[0]]))
print("Credit Card #: {}".format("".join(output)))
cv.imshow("Image", image)
cv.waitKey(0)
